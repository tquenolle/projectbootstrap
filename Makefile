.PHONY: all dev requirements

all:

dev: requirements

requirements:
	pip install -r config/requirements.txt
