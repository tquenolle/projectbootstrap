from django.conf import settings
from django.views.generic import TemplateView


class PartialView(TemplateView):
    def get_template_names(self):
        return [self.kwargs['template_name']]
