/* global angular */

'use strict';

// Declare app level module which depends on filters, and services
angular.module('MyProjectUI', [
  'ngCookies',
  'ngRoute',
]).config(['$locationProvider', function($locationProvider) {
  $locationProvider.html5Mode(true).hashPrefix('!');
}]).config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/', {
    templateUrl: 'partials/home.html',
  });
  $routeProvider.otherwise({redirectTo: '/'});
}]).run(['$http', '$cookies', function($http, $cookies) {
  $http.defaults.headers.common['X-CSRFToken'] = $cookies.csrftoken;
}]);

angular.bootstrap(document, ['MyProjectUI']);
