from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView

from myproject.views import PartialView


urlpatterns = patterns('',
    url(r'^(?P<template_name>partials/.*)$', PartialView.as_view()),
    url(r'^.*$', TemplateView.as_view(template_name='index.html'), name='index'),
)
